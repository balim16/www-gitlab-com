---
layout: handbook-page-toc
title: "Building and reinforcing a sustainable culture"
canonical_path: "/company/culture/all-remote/building-culture/"
description: Building and reinforcing a sustainable culture
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're detailing how to build, communicate, and reinforce a sustainable culture in a remote environment.

## How do you build and sustain culture in a remote environment?

![GitLab values illustration](/images/all-remote/gitlab-values-tanukis.jpg){: .medium.center}

"How do you build and sustain culture in a remote environment?" or "[How does culture work remotely?](/company/culture/#culture-at-gitlab)" are questions we frequently hear at GitLab. In colocated settings, culture is often implied, built from how team members treat one another, what is rewarded, what is chided, and what is deemed acceptable during in-person interactions.

Building a culture across a company where there are no offices requires **intentionality**. While technology and tools are enabling companies to operate efficiently in a remote setting, it's important to focus on documenting culture first, *then* using tools to support.

In colocated companies, it's easy to let culture be shaped by office decor, the neighborhood in which a company's headquarters is located, or the loudest voice in the room. Not only is this dangerous — one's culture can oscillate based on external factors — but it's not a usable strategy in a remote environment. 

In a remote team, there's no office vibe, hip coffee, or Spotify playlists that decide the culture, which is a gift. Instead, culture is written down. Culture is equal to the [values](/handbook/values/) you write down, and what you do as a leadership team to [reinforce](/handbook/values/#how-do-we-reinforce-our-values) those values. 

It starts by being intentional about [informal communication](/company/culture/all-remote/informal-communication/). 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/IU2nTj6NSlQ?" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [video](https://youtu.be/IU2nTj6NSlQ) above, GitLab's Head of Remote discusses the topic of scaling culture during an interview with [Stuart Miniman](https://twitter.com/stu/), host of [theCUBE](https://www.thecube.net/) and GM of Content at [SiliconANGLE Media](https://siliconangle.com/). A portion is transcribed below.*

> Remote is much better for your mental health and sanity than other settings, and it's because it forces you to work asynchronously. At GitLab, we have people spread across 65 countries, so almost every time zone is covered. But, that also means that someone on your team is likely in a vastly different timezone. In fact, they may be asleep the entire time you're up working.
>
> With an asynchronous mindset, it enables all of us to take a step back and assume that whatever we're doing is done with no one else online. It removes the burden of a nonstop string of messages where you're expected to respond to things immediately.
>
> From a mental health standpoint, when you have an entire company that embraces that, we're all given a little more breathing room to do really deep work that requires long periods of uninterrupted time.
>
> As a society, we're getting close to [a tipping point](https://medium.com/@gcvp/going-distributed-choosing-the-right-remote-friendly-team-model-6a04f833267c) where people are at their limit on how many more messages, or emails, or seemingly urgent pings they can manage while also doing their job well. We may be a bit ahead of the curve on that, but my hope is that the industry at large embraces asynchronous communication, and allows their people more time to actually do the work they were hired to do.

## Culture looks different in a pandemic

Do not conflate forced work-from-home during a pandemic with cultural degradation due to working in a remote environment. 

Many companies forced into remote by external factors such as COVID-19 are recognizing that, in general, employees are not as happy. This creates a response question: "How do you maintain culture in a suddenly-remote environment?"

That *is* a valid question, but it masks the actual question: "**How do you maintain culture in *any* work environment during a global pandemic?**"

In a period of tremendous external stress, culture is less about workplace rah-rah and more about intentionally reallocating that energy to serve society. Leaders must accept that the benchmark has changed. Expecting employees and managers to maintain a pre-crisis level of cheer during a pandemic can further deflate morale, and refusing to acknowledge this reality will make it more difficult for culture to recover post-crisis. 

Even a company's culture champions, the most enthusiastic of team members, are under unprecedented duress during a global crisis. Many are struggling to work while doubling as a homeschool teacher, or concerned about the wellbeing of neighbors and community members. The energy they once allotted to championing workplace culture is being used up elsewhere, rightly prioritized to focus on new stressors in life outside of work. Complicating matters further is that few employees are willing to state this for fear of being penalized for taking their focus away from work, and not living up to expectations. It is essential for leadership to proactively take [steps to create a non-judgmental culture](/company/culture/all-remote/mental-health/#creating-a-non-judgemental-culture). 

Rather than assuming that workplace culture is eroding, consider showcasing how people in your organization are using their energy to support others outside of the workplace. Hearing stories of colleagues supporting first-line workers, neighbors, and community members will bolster morale at work. Being open about taking time away from work to be there for others who are struggling in the midst of a pandemic will create even stronger bonds *at* work. 

## How do I assess culture fit remotely?

For hiring teams, a common challenge is this: "How do I assess culture fit remotely?"

To answer this question, you first need to unlearn a bit, and change your frame of mind. A hiring manager should *not* aspire to assess culture fit. Rather, you should aspire to assess *values fit*. 

For many, it is assumed that culture is simply the aura, energy, or vibe one gets when walking into an office. This is largely driven by decor and personas in the room. It is dangerous to allow company culture to be dictated by such factors, as this will create an oscillating culture that changes depending on mood or socioeconomic conditions. 

A company culture is a company's list of values. Culture is an assurance that each employee respects, admires, and feels invested in a company's values, and that leadership works to ensure values are not violated. As GitLab, a sub-value within our Diversity, Inclusion & Belonging  value is "[culture fit is a bad excuse](/handbook/values/#culture-fit-is-a-bad-excuse)."

Remote [interviewers](/company/culture/all-remote/interviews/) should link a company's values during the interview and have a conversation to assess a candidate's alignment and understanding of those values. Particularly in a remote setting, [values](/handbook/values/) serve as the north star, guiding every business decision by people you cannot physically see and shaping how colleagues treat one another. 

## No unwritten rules

There should be no unwritten rules in remote culture. Intentional documentation is essential to avoiding [dysfunction](/handbook/values/#five-dysfunctions) within a remote company, and this also applies to culture. At GitLab, this begins with our company values: [Collaboration](/handbook/values/#collaboration), [Results](/handbook/values/#results), [Efficiency](/handbook/values/#efficiency), [Diversity, Inclusion & Belonging ](/handbook/values/#diversity-inclusion), [Iteration](/handbook/values/#iteration), and [Transparency](/handbook/values/#transparency).

**Culture is best defined not by how a company or team acts when all is well; rather, by the behaviors shown during times of crisis or duress**.

## Intentional onboarding

A team member's first experience with company culture is unavoidable. The [onboarding experience](/company/culture/all-remote/learning-and-development/#how-do-you-onboard-new-team-members) serves as the first post-interview encounter with culture, and it is essential to infuse the [importance of values](/company/culture/all-remote/values/) into that experience.

Remote onboarding should set aside time for a new team member to read and digest a company's values, which serve as a company roadmap to culture. Consider having a mentor or [onboarding buddy](/handbook/people-group/general-onboarding/onboarding-buddies/) specifically ask questions related to values, providing opportunity for the new team member to dive deeper into how they are lived day-to-day.

### Using GitLab for remote onboarding

GitLab is a collaboration tool designed to help people work better together whether they are in the same location or spread across multiple time zones. Originally, GitLab let software developers collaborate on writing code and packaging it up into software applications. Today, GitLab has a wide range of capabilities used by people around the globe in all kinds of companies and roles.

You can learn more at GitLab's [remote team solutions page](/solutions/gitlab-for-remote/).

## Reinforcing your values

Whatever behavior you reward will become your values. New hires and promotions serve as important decisions to promote and reinforce values. GitLab reinforces its values by what:

1. [Leadership](/handbook/leadership/) does.
1. We select for during [hiring](/handbook/hiring/).
1. We emphasize during [onboarding](/handbook/people-group/general-onboarding/).
1. Behavior we give each-other [360 feedback](/handbook/people-group/360-feedback/) on.
1. Behavior we [compliment](/handbook/communication/#say-thanks).
1. Criteria we use for [discretionary bonuses](/handbook/incentives/#discretionary-bonuses).
1. Criteria we use for our [annual compensation review](/handbook/total-rewards/compensation/compensation-review-cycle).
1. Criteria we use for [promotions](/handbook/people-group/promotions-transfers/).
1. Criteria we use to [manage underperformance](/handbook/leadership/underperformance/).
1. We do when we [let people go](/handbook/people-group/offboarding/).
1. We give value awards for during [Contribute](/company/culture/contribute/).

In negative feedback, one should be specific about what the problem is. For example, saying someone is "[not living the values](https://hbr.org/2017/05/how-corporate-values-get-hijacked-and-misused)" isn't helpful.

## Never take culture for granted

While the importance of culture is driven home during onboarding, continual reinforcement is required to keep it top-of-mind. In the course of business, it's easy to lose sight of values and culture when focusing on [OKRs](/company/okrs/) and [KPIs](/handbook/ceo/kpis/). However, it is vital for leadership to remind themselves and other team members that values should never be pushed aside or lowered in priority.

*Every* decision a business makes should align with their values. Otherwise, values will be seen as "merely words," and culture will disintegrate.

If your company gets all team members together on a regular basis, consider resurfacing values or providing opportunities for groups to live out those values through community service. Just as certain trainings are recommended or required each year as part of a company's ongoing [learning and development](/company/culture/all-remote/learning-and-development/) efforts, reminding team members of values is vital to sustaining a strong culture.

## Gratitude and transparency

Persistent negativity can erode culture. While [feedback](/handbook/people-group/guidance-on-feedback/) is a gift, there's a fine line between reacting with hope and determination when facing a challenge and allowing a sense of apathy or dread to permeate a company. Leaders should be cognizant of this and act swiftly if there's a noted drop in outward gratitude or [transparency](/handbook/values/#transparency) in communications.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/cy6WGuzArgY?start=232" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

Emna G., founder and CEO at [Veamly](https://veamly.com/), spoke with GitLab's [Darren M.](https://twitter.com/darrenmurph) on how she fosters and sustains a thriving culture within a remote-first company.

> Every Monday, we get everyone on a Zoom call for something we call "Gratitude Monday." Everyone shares what they're grateful for. It doesn't have to be work-related. The idea is to live by gratitude; we need to learn how to embrace it.
>
> Some Mondays, you've had a bad weekend or you've woken up on the wrong foot. For me, I try to push my team to take a moment, pause, and really feel it. We see the difference when we remember what we're grateful for.
>
> We also do "Meditation Wednesdays," where we get on a Zoom call and we do breathing exercises. Then we share what we're stressed about, and what we're looking forward to. The idea is that it's OK to be stressed — we're human — but we want to balance that stress and create empathy.

## Spotlight culture ambassadors

A remote culture is only as strong as it is lived. At GitLab, we encourage team members to surface culture on a regular basis through the following mechanisms.

1. [Say Thanks](/handbook/communication/#say-thanks)
1. Use value emojis — ![CREDIT emoji](/images/handbook/values-emoji.png) — to recognize messages that exemplify a company value.
1. Recommend a colleague for a [discretionary bonus](/handbook/incentives/#discretionary-bonuses). If granted, the rationale and values lived are then documented and announced during a company call, serving as a beacon to others on how to improve the lives of others by living the values.
1. Serving as a GitLab [Contribute Ambassador](/events/gitlab-contribute/#ambassadors), which helps the entire company get the most out of a [weeklong trip](/company/culture/contribute/) to spend time with each other in person every 9-12 months.
1. Everyone can contribute to strengthening GitLab's values by creating [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/) and fostering discussion.

## Put structure around culture

It may sound counterintuitive, but there is great value in putting process and structure around culture. For example, if a company has an unlimited vacation policy, but has no suggestions or process around it, you may create a culture of fear with regard to taking time off.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/cy6WGuzArgY?start=1021" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

Emna G., founder and CEO at [Veamly](https://veamly.com/), spoke with GitLab's [Darren M.](https://twitter.com/darrenmurph) on the importance of process in building remote culture.

> Put process around things. Studies show that when a company has unlimited vacation, people don't take vacation.
>
> It's important to be communicative about the minimum that needs to be taken. It's also important to be clear on the structure. If you give a team something without structural limits, they really don't know how to navigate it.
>
> If you put structure around this, and you document it, you eliminate the guilt and the fear of missing out. — *Emna G., founder and CEO at [Veamly](https://veamly.com/)*

It's important for leadership to set the tone, but it's even more important to [document](/handbook/documentation/) what will define your culture. Each time a scenario arises where there is no clearly defined answer, look to your company values to determine the answer, and then document.

Documentation is a shared benefit, and is something that should be embraced by all members of the organization. While it may feel inefficient to document nuances related to culture, creating good habits around this will ensure that culture is as strong in the future as it was in a company's infancy.

## Close the office

Where feasible, consider closing the office and operating as a 100% remote company. This eliminates the possibility of any remote employee being seen, intentionally or unintentionally, as [deprioritized](/company/culture/all-remote/hybrid-remote/#disadvantages-to-hybrid-remote) within the organization.

If this is not feasible, and your company operates elsewhere on the [spectrum of remote](/company/culture/all-remote/stages/), ensure that your culture reinforces a remote-first way of doing business. It takes a concerted effort to transcribe hallway conversations into one's handbook or Slack channels, but ensuring that each employee is seen as a remote employee is the only way to ensure equal access to information.

## <%= partial("company/culture/all-remote/is_this_advice_any_good_remote.erb") %>

## Contribute your lessons

Creating and evolving company culture is something that all remote organizations must tackle. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page.

## GitLab Knowledge Assessment: Building and reinforcing a sustainable culture 

Anyone can test their knowledge on building and reinforcing a sustainable culture  by completing the [knowledge assessment](https://docs.google.com/forms/d/e/1FAIpQLSepPVtmDR49qoA3GyFRAI3R3pzNLpfn2w-mEnGn3O-JS67hHw/viewform). Earn at least an 80% or higher on the assessment to receive a passing score. Once the quiz has been completed, you will receive an email acknowledging the completion from GitLab with a summary of your answers. If you complete all knowledge assessments in the [Remote Work Foundation](/company/culture/all-remote/remote-certification/), you will receive an unaccredited [certification](/handbook/people-group/learning-and-development/certifications/). If you have questions, please reach out to our [Learning & Development team](/handbook/people-group/learning-and-development/) at `learning@gitlab.com`.

If you are a GitLab team member, please return to the Remote Work Certification in the GitLab Learn platform to take the quiz on this section. It will be the next smartcard in the pathway. 


----

Return to the main [all-remote page](/company/culture/all-remote/).
