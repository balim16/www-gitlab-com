---
layout: job_family_page
title: "Website Full Stack Developer"
---

The Website Full Stack Developer is on the Marketing team, and will work closely with Design, UX, Product Marketing, Content Marketing, and other members of the GitLab team. The role reports to the Senior Manager, Digital Experience. 

## Responsibilities
* Lead architecture and engineering (HTML, CSS, JS, Ruby, Middleman, Haml) of about.gitlab.com. 
* Run the marketing website as an open source project, optimizing for maximum contributions to both code and content.

## Requirements
* Expert knowledge of HTML, CSS, HAML and JavaScript (jQuery, Vue.js).
* Understanding of responsive design and best practices.
* The ability to iterate quickly and embrace feedback from many perspectives.
* Knowledge of information architecture, interaction design, and user-centered design.
* Knowledge of Git and comfortability using the command line.
* Experience with Jamstack, Ruby, and Middleman (and/or other static site generators).
* Ability to use GitLab.
* Previous experience with Static Site Generators like Middleman, Jekyll, Hugo, etc., preferred. 
* Experience working in a fully or partially remote company, preferred. 
* A positive outlook on changing priorities, preferred. 
* The ability to proactively question and improve priorities, preferred. 

## Levels

### Website Full Stack Developer (Intermediate) 

#### Website Full Stack Developer (Intermediate) Job Grade
The Website Full Stack Developer (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Website Full Stack Developer (Intermediate) Responsibilities
* Implement frameworks, code style guides, and templates to empower everyone to contribute.
* Implement site speed improvements and technical SEO.

#### Website Full Stack Developer (Intermediate) Requirements
* 3-5 years experience specializing in full stack development, website and web applications.

### Senior Website Full Stack Developer 

#### Senior Website Full Stack Developer Job Grade
The Senior Website Full Stack Developer is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades). 

#### Senior Website Full Stack Developer Responsibilities
* Extends the Website Full Stack Developer (Intermediate) responsibilities.
* Work with cross functional partners, acting as a team leader. 
* Critical decision making, and knowing what will have the biggest business impact when prioritizing. 
* Research and define frameworks, code style guides, and templates to empower everyone to contribute.
* Own CI automation using GitLab to build, test, and deploy.
* Sharing knowledge and educating on best practices and new technologies.

#### Senior Website Full Stack Developer Requirements
* 6+ years experience specializing in full stack development, website and web applications.
* Someone who uses research, data, and best practices to create, validate, and present ideas to key
stakeholders.
* A track record of being self-motivated, results oriented, and delivering on time.
* Expert in selecting and applying frameworks/systems to solve complicated technical problems. 
* The ability to communicate complex ideas and solutions to non-technical stakeholders. 

## Performance Indicators
* [Contributing to the success of Marketing's Quarterly Initiatives](https://about.gitlab.com/handbook/marketing/inbound-marketing/#q3-fy21-initiatives)
* [Identifying and organizing epics into executable Sprint plans](https://about.gitlab.com/handbook/marketing/growth-marketing/brand-and-digital-design/#sprint-planning)
* [Successfully completing weekly Sprint tasks](https://about.gitlab.com/handbook/marketing/growth-marketing/brand-and-digital-design/#sprint-cycle)
* [Collaborating on identifying issues to be completed within Epics](https://about.gitlab.com/handbook/marketing/inbound-marketing/#epics)
* [Effective Vendor Management](https://about.gitlab.com/handbook/marketing/growth-marketing/brand-and-digital-design/#vendor-management)

## Career Ladder

The next step in the Website Full Stack Development job family is not yet defined at GitLab. 

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, you can find their job title on our [team page](/company/team).

* Select candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 30 minute interview with our Senior Manager, Digital Experience. 
* Next, candidates will be invited to schedule a 30 minute technical interview with our Marketing Full Stack Developer and an interview with a Senior Brand Designer. 
* Next, candidates will be invited to schedule a 30 minute interview with the Senior Director, Growth Marketing. 
* Finally, candidates will be invited to schedule a 30 minute follow up interview with the Senior Manager, Digital Experience. 
* Successful candidates will subsequently be made an offer via phone or video. 

Additional details about our process can be found on our [hiring page](/handbook/hiring).
