---
layout: job_family_page
title: "Financial Planning and Analysis"
---

The FP&A team partners with executive team members across the company to provide timely, fact-based, data-driven decision support to help drive critical decisions. The team drives annual planning, owns the operating plan and long-term financial model, internal financial reporting, and financial and analytical tools to make GitLab predictable.

## Responsibilities

- Financial Acumen: Have a solid understanding of financial statements.
- Financial Modeling: Ability to understand or build bottom up financial models to plan, measure and forecast the business.
- Business Partnership: Engage with stakeholders regarding business strategy, go to market, functional and company strategy, spending initiatives, ad hoc financial analysis and monthly, quarterly and annual planning. Collaborate with accounting/data/IT teams on process improvement projects.
- Planning and Financial Analysis: Participate in annual planning, long-term planning, rolling forecast and variance process, make recommendations to improve the process. Explore investment options and present risk and opportunities.
- Data Analysis: Summarize key data driven insights to members of the executive team to drive better outcomes, an increase in revenue or decrease in cost. Participate in monthly key reviews for your functional area.
- Financial Process Improvement: Execute improvements to processes within your own workflow. Document in the company handbook.
- Communication: Prepare and review visualizations of financial data to promote internal and external understanding of the company’s financial results. Clearly articulate insights.
- Share our [values](/handbook/values/), and work in accordance with those values.

## Requirements

- BS degree in Finance, Accounting or Economics or relevant degree. MBA or relevant certification (e.g. CFA/CPA) is a plus.
- Financial Modeling: Be able to understand and update financial models that follow industry best practices. Expertise in Google sheets (we do not use excel for modeling purposes).
- Business Partnership: Consistent track record of using quantitative analysis to impact key business decisions.
- Data Analysis: A passion for understanding business questions and making data driven insights. Excellent analytical skills. SQL experience preferred.
- Communication: Ability to present financial data concisely using detailed reports and charts and through written and oral communication.
- Systems: Hands-on experience with financial and visualization software. Netsuite, Sisense and Adaptive Planning a plus.
- Ability to use GitLab.

## Levels

### FP&A Analyst (Intermediate)

The FP&A Analyst reports to the [Manager, FP&A](#manager-fpa).

#### FP&A Analyst (Intermediate) Job Grade

The FP&A Analyst is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### FP&A Analyst (Intermediate) Responsibilities

- Business Partnership: Support Senior Financial Analysts and Managers regarding business strategy, go to market, functional strategy, spending initiatives, ad hoc financial analysis and monthly, quarterly and annual planning.
- Financial Modeling: Maintain and improve financial models to plan, measure and forecast the business.
- Planning and Financial Analysis: Participate in annual planning, long-term planning, rolling forecast and variance process.
- Data Analysis: Produce KPI reporting and root cause trends in KPIs. Prepare insights for variance and key meetings.
- Project Management: Run small sized projects that improve our ability to make better data driven insights or make the company more efficient.
- Communication: Target audience to FP&A management, functional managers and directors. 

#### FP&A Analyst (Intermediate) Requirements

- 3-5 years of experience in a finance role ideally with enterprise SaaS software model.

### Senior FP&A Analyst

The FP&A Analyst reports to the [Manager, FP&A](#manager-fpa).

#### Senior FP&A Analyst Job Grade

The Senior FP&A Analyst is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior FP&A Analyst Responsibilities

- Extends the FP&A Analyst responsibilities.
- Project Management: Run medium-sized projects that improve our ability to make better data driven insights or make the company more efficient.
- Communication: Target audience CFO, CFO staff and a functional EVP.

#### Senior FP&A Analyst Requirements

- Extends the FP&A Analyst requirements.
- Business Acumen: Be able to understand the business at a level to understand EVP priorities.
- Experience owning a business function as a finance business partner.
- 5-7 years of experience in a finance role ideally with enterprise SaaS software model.

### FP&A Manager

The FP&A Manager reports to the [Manager, FP&A](#manager-fpa).

#### FP&A Manager Job Grade

The FP&A Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### FP&A Manager Responsibilities

- Extends the Senior FP&A Analyst responsibilities.
- Review: Review work and analysis produced by the team you manage or from other members of the team.
- Financial Process Improvement: Recommend improvements to processes and policies within the FP&A team.
- Project Management: Run large complex projects that improve our ability to make better data driven insights or makes the company more efficient.
- Communication: Target audience basic influence to CFO, CFO staff and functional EVPs.
- Mentorship: As a senior individual contributor, you mentor other team members in modeling, analysis and finance.  

#### FP&A Manager Requirements

- Extends the Senior FP&A Analyst requirements.
- Communication: Demonstrated an ability to influence business stakeholders.
- Experience as an expert owning a  business function as a finance business partner.
- 7-10 years of experience in a finance role ideally with enterprise SaaS software model.

### Manager, FP&A

The Manager, FP&A reports to the [Senior Manager, FP&A](#senior-manager-fpa).

#### Manager, FP&A Job Grade

The Manager, FP&A is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, FP&A Responsibilities

- Extends the FP&A Manager responsibilities.
- Management: Hire, build, coach and manage a highly productive team day to day. Learning the craft of management as a first level management role.

#### Manager, FP&A Requirements

- Extends the FP&A Manager requirements.
- 1-2 years of management experience in a financial role.
- 7-10 years of experience in a finance role ideally with enterprise SaaS software model.

### Senior Manager, FP&A

The Senior Manager, FP&A reports to [Director, FP&A](#director-fpa).

#### Senior Manager, FP&A Job Grade

The Senior Manager, FP&A is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Manager, FP&A Responsibilities

- Extends the Manager, FP&A responsibilities.
- Financial process improvement: Identify/influence changes in other teams (accounting, business function).
- Project Management: Run large cross-functional projects / working groups that improve our ability to make better data driven insights or makes the company more efficient.
- Management: Have a solid management skills and techniques especially around prioritization.

#### Senior Manager, FP&A Requirements

- Extends the Manager, FP&A requirements.
- 3-5 years of management experience in a financial role.
- Experience as an expert owning a large business function or multiple functions as a finance business partner.
- 10-12 years of experience in a finance role ideally with enterprise SaaS software model.

### Director, FP&A

The Director, FP&A reports to the [Senior Director, FP&A](#senior-director-fpa).

#### Director, FP&A Job Grade

The Director, FP&A is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, FP&A Responsibilities

- Extends the Senior Manager, FP&A responsibilities.
- Review: Expert at providing feedback work and analysis produced by the team.
- Financial Process Improvement: Identify and drive improvements to processes and policies within the FP&A team and in business partner groups (accounting, business function).
- Project Management: Drive system improvement initiatives.
- Management: Set the vision, hire, build, coach and manage a highly productive team day to day. Expert management skills, especially at developing talent. Set goals and priorities for the team. May manage managers.
- Be a culture definer and evolver of GitLab [values](/handbook/values/).

#### Director, FP&A Requirements

- Extends the Senior Manager, FP&A requirements.
- Communication: Expert at influencing business stakeholders.
- 5-7 years of management experience in a financial role.
- 12-15 years of experience in a finance role ideally with enterprise SaaS software model.

### Senior Director, FP&A

The Senior Director, FP&A reports to the [Vice President of Financial Planning and Analysis](/job-families/finance/vp-finance/).  

#### Senior Director, FP&A Job Grade

The Senior Director, FP&A is a [grade 11](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Director, FP&A Responsibilities

- Extends the Director, FP&A responsibilities.
- Communication: Target audience may include with Board Members and/or Audit Committee meetings.
- Management: Be a leader in management best practices among the FP&A leadership team.

### Senior Director, FP&A Requirements

- Extends the Director, FP&A requirements.
- Business Acumen: Be able to understand the business at a level to influence CEO and EVP priorities and company strategy.
- 7-10 years of management experience in a financial role.
- 15-18 years of experience in a finance role ideally with enterprise SaaS software model.

## Career Ladder

The next step in the Financial Planning and Analysis job family is to move to the [VP, FP&A](/job-families/finance/vp-finance/) job family.

## Performance Indicators

- [Budget, Forecast Creation Cycle Time](/handbook/finance/financial-planning-and-analysis/#budget-forecast-creation-cycle-time)
- [Plan vs Actual](/handbook/finance/financial-planning-and-analysis/#plan-vs-actual)
- [Team Morale Score](/handbook/finance/financial-planning-and-analysis/#team-morale-score)

## Hiring Process

Candidates for these positions can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the process at any stage of the interview plan.

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters,
- Next, candidates will be invited to schedule a 50 minute interview with our VP Finance,
- Next, candidates will be invited to schedule a 50 minute interview with one or two FP&A team members,
- Next, candidates may be invited to schedule a 50 minute interview with a People Ops team member,
- Next, candidates will be invited to schedule a 50 minute interview with our CAO,
- Next, candidates will be invited to schedule a 50 minute interview with the EVP of the function to support,
- Next, candidates will be invited to schedule a 50 minute interview with our CFO.

Additional details about our process can be found on our hiring page.
