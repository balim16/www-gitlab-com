---
layout: job_family_page
title: Strategy and Operations
---

They Strategy and Operations team are GitLab's internal strategy consultants,  who move from problem to problem in different functional areas. This team also plays an operational role in helping to support key intiatives while supporting the Chief of Staff and CEO. 

## Levels

### Staff Strategy and Operations 

The Staff Stratgy and Operations reports to the [Chief of Staff](/job-families/chief-executive-officer/chief-of-staff/).

#### Staff Strategy and Operations Job Grade

The Staff Strategy and Operations is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.

#### Staff Strategy and Operations Responsibilities

- Fill in knowledge gaps of the Chief of Staff
- Identify procedural gaps in existing workflows and work to resolve with optimization, automation, and data
- Support the creation of prepared materials (documents, decks) for the Chief of Staff and the CEO
- Translate practical needs into technical and/or business requirements
- Support special projects that are urgent and important
- Execute on projects and ongoing assignments for the Chief of Staff, as directed
- Demonstrate GitLab values in all work

#### Staff Strategy and Operations Requirements

- Detail-oriented forward thinker
- Excellent written, verbal, and technical communicator
- Experience making data-driven decisions
- Able to manage multiple tasks with competing timelines and deliverables
- Track record of leadership independent of role
- History of working cross-functionally to move projects forward
- Evidence of success in leading key business initiatives. Demonstrated ability to take a project from ideation through to implementation
- Experience at a strategy consulting firm is preferred
- Ideally, 2+ years experience in high growth startup environments
- Familiarity with the industry is a strong plus
- Ability to use GitLab

### Principal Strategy and Operations

The Principal Strategy and Operations reports to the [Chief of Staff](/job-families/chief-executive-officer/chief-of-staff/).

#### Principal Strategy and Operations Job Grade

The Principal Strategy and Operations is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.

#### Principal Strategy and Operations Responsibilities

* Extends that of the Staff Strategy and Operations responsibilities
- Collaborate cross-functionally with counterparts throughout the business 
- Identify opportunities to improve business efficiency and make changes to improve the business
- Translate practical needs into technical and/or business requirements

#### Principal Strategy and Operations Requirements

* Extends that of the Staff Strategy and Operations requirements
- Track record of success as an independent contributor or manager
- Ability to work collaboratively and drive results with junior and senior team members
- Strategic or operational work experience, preferably both
- 5+ years of work experience. Ideally, 2+ years experience in high growth startup environments

### Director of Strategy and Operations 

The Director of Startegy and Operations reports to the [Chief of Staff](/job-families/chief-executive-officer/chief-of-staff/).

#### Director of Strategy and Operations  Job Grade

The Director of Strategy and Operations is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.

#### Director of Strategy and Operations  Responsibilities

- Support special projects that are urgent and important. For example, market entries, pricing and packing projects, and organizational realignments
- Drive key organizational processes. For example, OKRs and strategic planning exercises
- Collaborate cross-functionally with senior counterparts throughout the business 
- Identify opportunities to improve business efficiency and make changes to improve the business
- Prepare materials (documents, decks) for the Chief of Staff and the CEO
- Translate practical needs into technical and/or business requirements
- Execute on projects and ongoing assignments for the Chief of Staff, as directed
- Demonstrate GitLab values in all work

#### Director of Strategy and Operations  Requirements

- Track record of leadership independent of role
- Ability to work collaboratively and drive results with senior leadership
- Strategic and operational work experience
- Detail-oriented forward thinker
- Excellent written, verbal, and technical communicator
- Experience making data-driven decisions
- Able to manage multiple tasks with competing timelines and deliverables
- History of working cross-functionally to move projects forward
- Evidence of success in leading key business initiatives. Demonstrated ability to take a project from ideation through to implementation
- Experience at a strategy consulting firm and/or working across multiple functions at a fast growing company
- 8+ years of work experience. Ideally, 4+ years experience in high growth startup environments
- Familiarity with the industry is a strong plus
- Ability to use GitLab

### Specialties

##### Strategy and Operations (Data)

* Extends that of the Staff or Principal level Strategy and Operations 
- Experience stepping into a new data source and preparing new analyses
- A familiarity with proxy metrics where actual measurements aren’t available
- Ability to guide conversations related to strategic choices of performance indicators

##### Strategy and Operations (Operations)

* Extends that of the Staff or Principal level Strategy and Operations 
- Experience leading ongoing projects and initiatives
- Proven ability to manage cross-functional projects
- Track record of moving key initiatives from idea to delivery
- Experience in setting goals and driving toward quantifiable results

## Performance Indicators

- [Meetings shifted from CEO - hours spent in meetings decreases overtime by Chief of Staff stepping in, supported by the [Chief of Staff Team](/handbook/ceo/chief-of-staff-team/performance-indicators/#executive-time-for-the-ceo)
- [Throughput - Issues or merge requests closed as measurement of projects iterated on](/handbook/ceo/chief-of-staff-team/performance-indicators/#throughput-for-the-cost)
- [Percent of Slack Messages that aren't Direct Messages](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/performance-indicators/#percent-of-sent-slack-messages-that-are-not-dms)

## Career Ladder

This is a cross-functional role, so team members will have exposure to broader parts of the organization. They may choose to join other teams. They may also be considered for the [Chief of Staff role](https://about.gitlab.com/job-families/chief-executive-officer/chief-of-staff/) as the Chief of Staff completes the rotation. 

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](https://about.gitlab.com/company/team/)

- Qualified candidates will be invited to schedule a 30 minute screening call with one of our global recruiters
- Next, candidates will be invited to schedule a 50 minute interview with a team member from another department
- Then, candidates will be invited to schedule a 50 minute interview with the Chief of Staff and be asked to complete a homework assignment
- Next, candidates will be invited to schedule a 50 minute interview with a member of our Engineering team and be asked to complete a product assessment
- Then candidates will be invited to schedule a 50 minute interview with our CLO or another member of the executive team
- Finally, candidates will meet with our CEO who will conduct the final interview

As always, the interviews and screening call will be conducted via a video call. See more details about our hiring process on the [hiring handbook](https://about.gitlab.com/handbook/hiring/).
