---
layout: handbook-page-toc
title: "Ideal Customer Profile"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Ideal Customer Profile
An ideal customer profile is the description of our "perfect" customer company (not individual or end user).  The profile takes into consideration firmographic, environmental and additional factors to develop our focus list of highest value accounts.

**A few things to note about GitLab's ICP:** 
- it is that it is fairly broad, mainly because GitLab can ultimately sell to a vast number of companies versus say, a banking solution that would have a much smaller TAM (total addressable market)  
- our ICP is hyper focused on first order logos
- Because of our large total addressable market (TAMkt) we do not focus on ALL the accounts that fit our ICP at a given time, but rather, focus on a subset based on different variables including propensity to buy and additional intent data.  We do this by creating Focus Account Lists for both Large and Mid-Market.  More details on this can be found on the [Focus Account List page](/marketing/revenue-marketing/account-based-strategy/focus-account-lists/).

### Large

|  | **Attribute** | **Description** |
| ------ | ------ | ------ |
| **Core criteria (must haves)** | Number of developers (currently also using company size as proxy) | 500+ |
| | Tech stack (regional) | Includes GitHub, Perforce, Jenkins, BitBucket or Subversion |
| | Cloud provider | AWS or GCP |
| | Prospect | First order logo (not a current PAID customer for GitLab anywhere within the organization)  |
| **Additional criteria (attributes to further define)** | Digital transformation | identified C-suite initiative | 
| | High intent account | Account is trending as high intent based on our data in Demandbase |

### Mid Market
There are two ideal customer profiles for Commercial, one for >500 employees and one for <500 employees.

|  | **Attribute** | **Description** |
| ------ | ------ | ------ |
| **Core criteria (must haves)** | Number of employees | >500 |
| | Tech stack (regional) | Includes GitHub, Perforce, Jenkins, BitBucket or Subversion.  Also include a LACK of tech stack in smaller companies |
| | Prospect | First order logo (not a current PAID customer for GitLab anywhere within the organization)  |
| **Additional criteria (attributes to further define)** | New hire | CIO | 
| | High intent account | Account is trending as high intent based on our data in Demandbase 

|  | **Attribute** | **Description** |
| ------ | ------ | ------ |
| **Core criteria (must haves)** | Number of employees | <500 |
| | Tech stack (regional) | A lack of technology installed.  GitLab would be the first purchsed for Devops for these companies. |
| | Prospect | First order logo (not a current PAID customer for GitLab anywhere within the organization)  |
| **Additional criteria (attributes to further define)** | New hire | CIO | 
| | High intent account | Account is trending as high intent based on our data in Demandbase
