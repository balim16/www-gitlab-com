---
layout: handbook-page-toc
title: "PTY LTD Benefits"
description: "GitLab PTY Australia benefits specific to Australia based team members."
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## GitLab PTY Australia Specific Benefits

### Medical
GitLab does not plan to offer Private Health Insurance at this time because Australians can access the public health system for free or at a lower cost through Medicare (funded by tax).

Please note, Medicare doesn't cover all costs and [other health services](https://www.servicesaustralia.gov.au/individuals/subjects/whats-covered-medicare/health-care-and-medicare). Depending on the treatment, tests required and medications, individuals may still have to pay out of pocket. Some individuals may also have to pay a [Medicare levy and a Medicare levy surcharge](https://www.ato.gov.au/Individuals/Medicare-levy/). These individuals can avoid the surcharge by taking out private health insurance.

### Superannuation
GitLab will make superannuation contributions directly to the team member's nominated super fund according to the Australian Government Super Guarantee rate which is currently set at 9.5% of the team member's total salary. Super is on top of the salary listed in the compensation calculator/contract.

#### Superannuation payments due dates

Super payment due dates has been a common topic in [#loc_australia](https://app.slack.com/client/T02592416/CHHFS9DR7). However, there is no reason to worry when payments do not happen monthly as they may be done quarterly.

The Australian Taxation Office (ATO) states the following:

> You pay super for eligible employees calculated from the day they start with you. You must make the payments at least four times a year, by the quarterly due dates.

Payments due dates can be found on the [ATO website](https://www.ato.gov.au/business/super-for-employers/paying-super-contributions/when-to-pay-super/).

#### Superannuation Salary Sacrifice
Team members in Australia have the option to make concessional super contributions by sacrificing some of their pre-tax pay and arrange to have it paid into their superfund instead. The combined total of GitLab and salary sacrificed contributions [must not be more than $25,000](https://moneysmart.gov.au/grow-your-super/super-contributions) per financial year.

*Salary Sacrifice FAQ*

* How do I make concessional contributions to my superannuation?
  * Email total-rewards@gitlab.com with the percentage or amount of your salary that you would like to sacrifice.
* Can I change the amount or opt out?
  * Yes, if you wish to change the amount/percentage or opt out, simply send total-rewards@gitlab.com an email.
* Is it possible to start from any month?
  * Yes, it would be processed on the next available payroll.


### Life insurance
GitLab does not plan to offer life insurance at this time as Australians can access [government payments and services](https://www.humanservices.gov.au/individuals/subjects/payments-people-living-illness-injury-or-disability) if they get ill, injured or have a disability. Most Australians who choose to have life insurance take out cover from their super fund.

### GitLab PTY Australia Annual, Sick and Carer's Leave
**Annual Leave**
* Team members are entitled to 4 weeks of paid leave each year, which runs concurrently with GitLab PTO and accumulates from the first day of employment. Any unused annual leave rolls over to the following year.
* Team members in Australia are eligible to take paid time off according to our [PTO policy](https://about.gitlab.com/handbook/paid-time-off/). Since GitLab has a legal duty to pay out any accumulated Annual Leave when a team member leaves the company, the payroll team will deduct vacation time off requests from the team member's Annual Leave balance. When taking vacation time, team members must select the `Vacation` option in PTO by Roots to ensure this time is properly tracked.  Annual leave does not expire and may be carried over year to year.  For team members who are not covered by a modern award or enterprise agreement, they may be asked to take a period of paid annual leave if such request is reasonable, such as when a team member has accrued an excessive amount of paid annual leave.

**Sick Leave**
* Team members are entitled to 10 days of sick/carer's leave each year. This leave runs concurrently with GitLab PTO.
* Sick and carer's leave, also known as personal/carer's leave fall under the same statutory leave entitlement. Selecting the `Out Sick` option in PTO by Roots will prompt the payroll team to process any time off taken as Sick and Carer's Leave. If you wish to take Carer's Leave but don't want to change your Slack status to `Out Sick`, send your Carer's Leave request to total-rewards@gitlab.com and the Total Rewards team will manually add your leave to BambooHR and notify payroll.

### On-Call (Engineering only)

Certain groups in the Engineering division are expected to participate in on-call for business continuity and/or customer support. For more details on the on-call procedure, please reference the [on-call page](https://about.gitlab.com/handbook/on-call/). The purpose of this section is to provide an overview of **time in lieu that will be offered for the Development and Infrastructure departments effective February 1, 2021.** We will continue to review and implement to other departments within Engineering iteratively.

With our global team, we strive for a "follow the sun" model for on-call rotation. This way, team members can participate in on-call during their daytime rather than their nighttime in their respective regions (APAC, EMEA and AMER). In APAC, the majority of the team members are located in Australia. We want to ensure engineers in Australia are 1) eligible for on-call shifts, and 2) stay aligned with local laws to ensure proper rest/recovery time after on-call shifts.

Engineers based in Australia who are part of the Development and Infrastructure departments will be eligible for time in lieu after on-call shifts. Time in lieu is time off from work that is granted to team members to work outside of standard working hours (I.E. ordinary hours).

##### How much time in lieu will I receive for volunteering for on-call shifts?

* 4-hour shift = ½ day time in lieu
* 8-hour shift = 1 day time in lieu
* 24-hour shift = 3 days time in lieu 

For weekend shifts please be particularly mindful about managing your time to **ensure you are not exceeding a 38 hour work-week**, and feel free to leverage our [unlimited PTO policy](https://about.gitlab.com/handbook/paid-time-off/#paid-time-off) to complement time in lieu. 

_Note: The full shift (state of readiness) is compensated by time in lieu - rather than only the "actual time worked"_

##### What should I do when I want to take more time off after an on-call shift?

If you need additional time to ensure a good work/life balance, on top of the time in lieu, you can leverage our Unlimited [PTO policy as described here](https://about.gitlab.com/handbook/paid-time-off/#paid-time-off). 

##### When can time in lieu be taken?

Time in lieu should be taken _within 2 weeks after completion of the on-call shift_. We want to ensure that time in lieu is taken close to the on-call shift to ensure that team members have sufficient time to rest.

##### Do all on-call shifts result in time in lieu?

No, only on-call shifts outside of working hours (I.E. weekends, evenings, etc.). _Please note that not all teams require weekend or evening on-call shifts, so this may not be applicable for all._

##### How do I request time in lieu?

Time in lieu should be requested via [PTO by Roots](/handbook/paid-time-off/#pto-by-roots) by selecting the `On-Call Time in Lieu` option.


### GitLab PTY Australia Parental Leave

#### Statutory Leave Entitlement
All team members are entitled to up to 12 months of unpaid parental leave if they have been at GitLab for at least 12 months. They can also request up to 12 months of _additional_ unpaid parental leave. 

#### Maternity Leave Payment
* For team members with a yearly salary of $150,000 AUD or less:
  * Eligible team members who are the primary carer of a newborn or adopted child can receive up to 18 weeks of government-funded Parental Leave Pay via [Centrelink](https://www.humanservices.gov.au/individuals/services/centrelink/parental-leave-pay), which is paid at the national minimum wage. 
  * If you are [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), GitLab will supplement the government-funded payment to ensure that you receive 100% pay for up to 16 weeks of your Parental Leave. 
  * To receive the government-funded Parental Leave payment, please submit an application via [Centrelink](https://www.humanservices.gov.au/individuals/services/centrelink/parental-leave-pay/claiming).
  * Total Rewards will have to "accept" the Parental Leave Pay for team members via [Business Hub](https://www.servicesaustralia.gov.au/organisations/business/services/centrelink/paid-parental-leave-scheme-employers/what-you-need-know/your-role-scheme/accept-parental-leave-pay-employees).
  * Parental Leave Pay can be claimed in one set period and one flexible period:
    * The first (set) period consists of 12 weeks and must be taken in one continuous period within 12 months of the birth or adoption of a child.
    * The second (flexible) period consists of 30 days and must be taken within 24 months of the birth or adoption of a child.
* For team members with a yearly salary above $150,000 AUD:
  * If you are [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), you will receive 100% paid Parental Leave from GitLab for up to 16 weeks.


#### Paternity Leave Payment
* Team members with a yearly salary of $150,000 AUD or less:
  * Eligible team members can receive up to 2 weeks of government-funded Dad and Partner pay via [Centrelink](https://www.servicesaustralia.gov.au/individuals/services/centrelink/dad-and-partner-pay), to be paid at the national minimum wage.
  * To receive the government-funded Parental Leave payment, please submit an application via [Centrelink](https://www.servicesaustralia.gov.au/individuals/services/centrelink/dad-and-partner-pay/how-claim).
    * If approved, team members will receive payment directly from the Australian Government.
    * If you are [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), GitLab will supplement the government-funded payment to ensure that you receive 100% pay for up to 16 weeks of your Parental Leave. 
* Team members with a yearly salary above $150,000 AUD:
  * If you are [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), you will receive 100% paid Parental Leave from GitLab for up to 16 weeks.


#### Applying for Parental Leave in Australia
* If you are applying for government-funded Parental Leave Pay, you can apply up to 3 months before your child's due date.
* To initiate your parental leave, submit your time off by selecting the `Parental Leave` category in PTO by Roots at least 30 days before your leave starts. Please familiarize yourself with [GitLab's Parental Leave policy](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).

**Record-keeping for paid Parental Leave:**
* In addition to the usual record-keeping requirements, employers that have team members getting government-funded Parental Leave Pay also have to keep the following records:
  * the amount of Parental Leave Pay funding received from the government for each team member and the period it covers
  * the date each parental leave payment was made to the team member
  * the period each payment covers
  * the gross amount of the payment
  * the net amount paid and the amount of income tax withheld (including other payments, if any, were made)
  * a statement identifying the payment as Parental Leave Pay under the Australian Government Paid Parental Leave Scheme
  * the amount of any deductions made from each payment.

**Pay slips for Parental Leave payments:**
* Team members who get Parental Leave Pay have to be given a pay slip for each payment. The pay slip must specify that the payments are Parental Leave Pay under the Australian Government Paid Parental Leave Scheme.
* Ordinary pay slip requirements apply to pay slips given to team members getting government-funded Parental Leave Pay. They must also include:
  * the gross and net amounts of Parental Leave Pay and the amount of income tax deducted
  * if there are other payments on the pay slip, this information must be included as well as the total gross, net and income tax amounts
  * the amount of any deduction and the name and bank details of the entity the deduction was given to.
    * Only certain deductions can be made from Parental Leave Pay under the Australian Government Paid Parental Leave Scheme.


##  GitLab PTY New Zealand Specific Benefits

### Accounting Fee Reimbursement
New Zealand team members are eligible for a one time reimbursement of up to $750 NZD for accounting or tax-related fees. The purpose of this reimbursement is to help our team members reconcile any accounting activities and tax returns from when they were independent contractors through GitLab BV or contracted through CXC. Since tax-related activities will be less complex after the transition to our team members' employment under the PTY LTD entity, this reimbursement benefit will only be valid until December 2021. To get reimbursed for this expense, please follow the [Expense Reimbursement process](/handbook/spending-company-money/#expense-policy).

### Annual Leave
Team members are entitled to a minimum of four weeks’ annual leave per year. Annual leave does not expire and can be carried over each year, but GitLab may set expectations on how much annual leave employees may accumulate and may require employees to take entitled leave with 14 days notice. Annual leave runs concurrently with GitLab PTO. Team members must select the `Vacation` option in PTO by Roots when taking vacation time to ensure that annual leave entitlement is properly tracked.

### Sick Leave

Team members in New Zealand are eligible to take paid time off according to our [PTO policy](https://about.gitlab.com/handbook/paid-time-off/). Team members in New Zealand are entitled to 5 days' sick leave after six months’ current continuous employment with GitLab. For each 12-month period after meeting the above criteria, each team member gets at least five days’ sick leave. If in any year the team member doesn’t meet the criteria, then they don’t get any new sick leave entitlement, but can use their sick leave balance which may have carried over. An employee may re-qualify for sick leave as soon as they meet the criteria.  The maximum amount of sick leave that can be accumulated under the Holidays Act 2003 is 20 days.

* Sick Leave runs concurrently with GitLab PTO. Team members must designate any time off for illness as `Out Sick` in PTO by Roots to ensure that sick leave entitlement is properly tracked.

* In accordance with the Holidays Act, GitLab may require a medical certificate if you have been absent due to illness. 

* Unused sick leave will not be paid out to the team member upon termination of employment.

**Injured Leave**
* When the team member is taking leave for the first week of a non-work accident, they can use sick leave and/or annual leave if they have any. This leave runs concurrently with GitLab PTO. Team members must designate time off for injury as `Out Sick` or `Vacation` in PTO by Roots if they wish to use sick leave or annual leave for the first week of an injury. Team members must notify Total rewards (total-rewards@gitlab.com) as soon as possible when they will need to go out on injured leave.

* If the leave will last longer than five days and is [covered by the Accident Compensation Corporation (ACC) scheme](https://www.acc.co.nz/im-injured/what-we-cover/), GitLab will top up the ACC payment from 80 to 100% for the first 25 days.

### GitLab PTY New Zealand Parental Leave 

#### Statutory Leave Entitlement
Team members can take up to 52 weeks of unpaid parental leave if they have been with GitLab for at least 12 months. They can also request up to an additional 26 weeks of unpaid parental leave.

#### Maternity Leave Payment
* Team members can receive government-funded Parental Leave pay for up to 26 weeks.
* If you are [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), GitLab will supplement the government-funded payment to ensure that you receive 100% pay for up to 16 weeks of your Parental Leave. 
* To apply for government-funded Parental Leave pay, please submit your application via [IRD](https://www.ird.govt.nz/paid-parental-leave/apply). Total Rewards will have to fill out the "Employer Declaration" part of the [form](https://www.ird.govt.nz/-/media/project/ir/home/documents/forms-and-guides/ir800---ir899/ir880/ir880-2020.pdf).

#### Paternity Leave Payment
* If you are [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), you will receive 100% paid Parental Leave from GitLab for up to 16 weeks.

#### Applying for Parental Leave in New Zealand
To initiate your parental leave, submit your time off by selecting the `Parental Leave` category in PTO by Roots at least 30 days before your leave starts. Please familiarize yourself with [GitLab's Parental Leave policy](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).

### Medical
GitLab does not plan at this time to offer Private Health Insurance benefits because New Zealand residents can access free or subsidised medical care in New Zealand through the public healthcare system. Injuries and accidents are covered by the Accident Compensation Corporation cover.

### Pension
GitLab's KiwiSaver contributions will be 3% on top of base salary. Team members will automatically be enrolled in the KiwiSaver scheme but may elect to opt out within the first 56 days of employment. GitLab will deduct a participating team member's contributions from their before-tax pay at the team member's chosen contribution rate (3%, 4%, 6%, 8% or 10%). If a team member does not choose a contribution rate, the default rate of 3% will be used to calculate deductions.

### Life Insurance
GitLab does not plan at this time to offer Life Insurance benefits because New Zealanders can access [government payments and services](https://www.workandincome.govt.nz/providers/health-and-disability-practitioners/health-and-disability-related-benefits.html) to help if they get ill, injured or have a disability.
