description: Follow these ten steps to seamlessly integrate security into DevOps
canonical_path: /topics/application-security/devsecops-security-checklist/
parent_topic: application-security
file_name: devsecops-security-checklist
twitter_image: /images/opengraph/gitlab-blog-cover.png
title: A DevSecOps security checklist
header_body: Your team has embraced the DevOps methodology and you’re ready to
  shift security left, moving it closer to developers. Developers are more
  likely to find and fix bugs if it’s easy for them to do so right in their
  workflow. But changing long-held beliefs and biases about security requires
  planning, patience, and persistence.
related_content:
  - title: What is fuzz testing?
    url: /topics/application-security/what-is-fuzz-testing/
  - title: Why developer-first security is important to understand
    url: /topics/application-security/what-is-developer-first-security.index.html
  - title: DevSecOps with GitLab
    url: /solutions/dev-sec-ops/
cover_image: /images/topics/application-security.svg
body: >-
  Here’s a ten-step DevSecOps security checklist that can help any team get on
  the same page.


  **1. Understand the challenge:** Our [2020 Global DevSecOps Survey](/developer-survey/) shows the divide between developers and security remains wide and filled with finger pointing. Over 42% of those surveyed said security testing happens too late in the lifecycle and over 60% of developers don’t run basic static application security testing (SAST) scans. There isn’t even agreement on who is actually responsible for security: 33% of security pros said they are, but 29% said everyone is. The bottom line: confusion abounds.


  **2. Explain the goal:** With so many different assumptions about security and ownership, offering clear goals to the team will provide something tangible to work towards. A successful DevSecOps practice will mean more code gets tested, which will translate into faster releases once things are going smoothly. DevSecOps can also improve planning: Bringing a security champion in from the beginning will mean security is involved in every step of the process, reducing friction and ultimately speeding up release cycles. And finally, DevSecOps will improve accountability even among non-security team members by creating a culture where “security is everyone’s responsibility.”


  **3. Measure:** It’s helpful to understand how much time a team wastes dealing with vulnerabilities after code is merged. Next, look for a pattern in the type or source of those vulnerabilities, and make adjustments for improvement.


  **4. Find the friction:** Bring everyone together for a frank discussion about the pain points and bottlenecks between development and security. Once everything is on the table, create a plan to resolve them, and then execute that plan.


  **5. Take small bites:** At GitLab, [iteration](/handbook/values/#iteration) is one of our core values, so when we make changes, we make tiny, quick-to-accomplish alterations and then build on them. The same principle is true when shifting from DevOps to DevSecOps: Make incremental code changes. Smaller updates are easier to review and secure, and can be launched more quickly than monolithic project changes.


  **6. Automate and integrate:** This is key for DevOps, but automation and [integration](/topics/ci-cd/implement-continuous-integration/) are also what make security scans a powerful tool. If scans are ubiquitous, every code change will be reviewed and vulnerabilities will be found much earlier in the process. The scans must be built into the developer’s workflow. Integrated security enables developers to find and fix vulnerabilities before the code ever leaves their hands. This also reduces the volume of vulnerabilities sent to the security team, streamlining their review.


  **7. Share the wealth:** Give developers access to SAST and DAST reports. While this is important for remediation, it’s also a valuable tool to help developers build secure coding practices.


  **8. Do a status check:** If you have any <a href="https://www.umsl.edu/~hugheyd/is6840/waterfall.html" target="_blank">waterfall-style</> security processes within your SDLC, this is the chance to eliminate or at least greatly reduce your dependence on them. You should always be able to change direction as needs arise: Keep your organization nimble.


  **9. Empower the security team:** Give the security team visibility into both resolved and unresolved vulnerabilities, where the vulnerabilities reside, who created them, and their status for remediation.


  **10. Streamline your tools:** A DevOps platform providing an integrated set of tools lets the team learn a single interface, rely on a single source of truth, and streamlines automation, integration, monitoring, and data delivery.
